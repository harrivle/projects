#!/afs/nd.edu/user15/pbui/pub/anaconda-2.3.0/bin/python

import sys, os, socket, getopt, logging, time

URL = 'www.example.com'
HOST = 'example.com'
PORT = 80
PATH = '/'
REQUESTS = 1
PROCESSES = 1
LOGLEVEL = logging.INFO

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: thor.py [-r REQUESTS -p PROCESSES -v] URL

Options:

	-h\t\t\tShow this help message
	-v\t\t\tSet logging to DEBUG level

	-r REQUESTS\t\tNumber of requests per process (default is 1)
	-p PROCESSES\tNumber of processes (default is 1)'''
	sys.exit(exit_code)

def parseURL(url):
	url = url.split('://')[-1]
	#url = url.split('?')[0]
	if '/' not in url:
		path = '/'
	else:
		path = url.split('/', 1)
		url = path[0]
		path = '/' + path[-1]
	if ':' not in url:
		port = 80
	else:
		port = int(url.split(':', 1)[-1])
		url = url.split(':', 1)[0]
	host = url.split('www.')[-1]

	return url, host, path, port

class HTTPClient(object):
	def __init__(self, logger, url = URL, host = HOST, path = PATH, port = PORT,):
		self.logger = logger
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.url = url
		self.host = host
		self.path = path
		self.port = port

	def handle(self):
		self.logger.debug('Handle')
		raise NotImplementedError

	def run(self):
		try:
			self.socket.connect((self.url, self.port))
		except socket.error as e:
			self.logger.error('Could not connect to {}:{}: {}'.format(self.url, self.port, e))
			sys.exit(1)

		self.logger.debug('Connected to {}:{}...'.format(self.url, self.port))

		try:
			self.handle()
		except Exception as e:
			self.logger.exception('Exception: {}', e)
		finally:
			self.finish()

	def finish(self):
		self.logger.debug('Finish')
		try:
			self.socket.shutdown(socket.SHUT_RDWR)
		except socket.error:
			pass
		finally:
			self.socket.close()

class ThorClient(HTTPClient):
	def handle(self):
		self.logger.debug('Handle')

		try:
			self.logger.debug('Sending Request')
			request = "GET {} HTTP/1.0\nHost: {}\n\n".format(self.path, self.host)
			self.socket.send(request)

			self.logger.debug('Receiving Request')
			result = self.socket.recv(128000)
			while (len(result) > 0):
				sys.stdout.write(result)
				result = self.socket.recv(128000)
		except socket.error:
			pass

if __name__ == '__main__':
	avgtime = 0

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hvr:p:")
	except getopt.GetoptError as e:
		usage(1)

	for opt, arg in opts:
		if opt == '-h':
			usage()
		elif opt == '-v':
			LOGLEVEL = logging.DEBUG
		elif opt == '-r':
			REQUESTS = int(arg)
		elif opt == '-p':
			PROCESSES = int(arg)
		else:
			usage(1)

	if len(args) == 1:
		URL, HOST, PATH, PORT = parseURL(args[0])

	logging.basicConfig(
		level = LOGLEVEL,
		format = '[%(asctime)s] %(message)s',
		datefmt = '%Y-%m-%d %H:%M:%S',
	)
	logger = logging.getLogger()
	logger.debug('URL: {}'.format(URL))
	logger.debug('HOST: {}'.format(HOST))
	logger.debug('PATH: {}'.format(PATH))
	logger.debug('PORT: {}'.format(PORT))

	for i in range(PROCESSES):
		starttime = time.time()
		pid = os.fork()

		if pid == 0:
			for j in range(REQUESTS):
				client = ThorClient(logger, URL, HOST, PATH, PORT)
	
				try:
					client.run()
				except KeyboardInterrupt:
					sys.exit(0)
				finally:
					sys.exit(0)
		elif pid > 0:
			os.wait()
			avgtime = (avgtime + (time.time() - starttime))/2
			logger.debug('{0} | Elapsed time: {1:.2f} seconds'.format(pid, time.time() - starttime))
			logger.debug('{0} | Average elapsed time: {1:.2f} seconds'.format(pid, avgtime))
		else:
			logger.debug('Forking Error')
			sys.exit(1)