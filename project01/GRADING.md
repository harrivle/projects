Project 01 - Grading
====================

**Score**: 6.25 / 20

Deductions
----------

* Thor

    - 0.5   Missing error checking of system calls (fork)
    - 0.25  Child should exit after processing all requests (you exit after 1 request)
    - 1.0   Not fully concurrent (should fork all processes, then wait for all of them)

* Spidey

    - 8.0   Missing

* Report

    - 4.0   Missing

Comments
--------
