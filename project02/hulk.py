#!/afs/nd.edu/user15/pbui/pub/anaconda-2.3.0/bin/python

import sys, getopt, string, hashlib, itertools

def usage(exit_code = 0):
	print >>sys.stderr, '''Usage: hulk.py [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]

Options:

	-a\tALPHABET\tAlphabet used for passwords
	-l\tLENGTH\t\tLength for passwords
	-s\tHASHES\t\tPath to file containing hashes
	-p\tPREFIX\t\tPrefix to use for each candidate password
	-h\t\t\tShow this help message'''
	sys.exit(exit_code)

def md5sum(s):
	return hashlib.md5(s).hexdigest()

a = list(string.ascii_lowercase + string.digits)
l = 8
s = './hashes.txt'
p = ''
perms_list = []
perms_dict = {}

opts, args = getopt.getopt(sys.argv[1:], 'a:l:s:p:h')

for opt, arg in opts:
	if opt in ('-a'):
		a = arg
	elif opt in ('-l'):
		l = int(arg)
	elif opt in ('-s'):
		s = arg
	elif opt in ('-p'):
		p = arg
	elif opt in ('-h'):
		usage()
	else:
		usage(1)

hashes = [line.rstrip() for line in open(s)]
perms_list = itertools.product(a, repeat = l)
perms_dict = {}

for i in perms_list:
	perms_dict[md5sum(p + ''.join(i))] = p + ''.join(i)

for h in hashes:
	if h in perms_dict:
		print perms_dict[h]