#!/afs/nd.edu/user15/pbui/pub/anaconda-2.3.0/bin/python

import sys, os, work_queue, json, itertools, string

queue = work_queue.WorkQueue(work_queue.WORK_QUEUE_RANDOM_PORT, name='hulk-hle1', catalog=True)
queue.specify_log('fury.log')
JOURNAL = json.load(open('journal.json'))

alpha = list(string.ascii_lowercase + string.digits)
pref = '0'

for i in range(1, 9):
	length = i
	if i > 6:
		length = 6
		pref = list(itertools.product(alpha, repeat = i - length))

	for p in pref:
		command = './hulk.py -l ' + str(length)
		if len(pref) > 1:
			command = command + ' -p ' + ''.join(p)

		if command in JOURNAL:
			print >>sys.stderr, 'Already did', command
		else:
			task = work_queue.Task(command)
		
			for source in ('hulk.py', 'hashes.txt'):
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
		
			queue.submit(task)

while not queue.empty():
	task = queue.wait()

	if task and task.return_status == 0:
		JOURNAL[task.command] = task.output.split()
		with open('journal.json.new', 'w') as stream:
			json.dump(JOURNAL, stream)
		os.rename('journal.json.new', 'journal.json')

		sys.stdout.write(task.output)
		sys.stdout.flush()