Project 02 - Grading
====================

**Score**: 18.25 / 20

Deductions
----------

* Hulk

    - 0.5   Fails test

* Fury

    - 0.25  Need to handle the case where JOURNAL doesn't exist

    *       No need to convert everything to a list (actually much more inefficient)

* Readme

    - 1.0   Not enough passwords

Comments
--------
